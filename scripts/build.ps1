#!/usr/bin/env bash
##~---------------------------------------------------------------------------##
##                        _      _                 _   _                      ##
##                    ___| |_ __| |_ __ ___   __ _| |_| |_                    ##
##                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   ##
##                   \__ \ || (_| | | | | | | (_| | |_| |_                    ##
##                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   ##
##                                                                            ##
##  File      : build.ps1                                                     ##
##  Project   : el_jamon_volador                                              ##
##  Date      : Feb 02, 2022                                                  ##
##  License   : GPLv3                                                         ##
##  Author    : stdmatt <stdmatt@pixelwizards.io>                             ##
##  Copyright : stdmatt - 2020                                                ##
##                                                                            ##
##  Description :                                                             ##
##                                                                            ##
##---------------------------------------------------------------------------~##

$GAME_NAME        = "El Ramon Volador";
$GAME_SIMPLE_NAME = "el_ramon_volador";

$SCRIPT_DIR    = (Split-Path -Parent $MyInvocation.MyCommand.Path);
$ROOT_DIR      = (Split-Path -Parent $SCRIPT_DIR);
$MODULES_DIR   = (Join-Path $ROOT_DIR "modules");
$SOURCE_DIR    = (Join-Path $ROOT_DIR "source");
$BUILD_DIR     = (Join-Path $ROOT_DIR "bin");

$EXE_NAME   = ($GAME_SIMPLE_NAME + ".gb");

##
## Configure GBDK Paths.
##
$GBDK_ROOT  = (Join-Path $MODULES_DIR "gbdk");
if($IsWindows) {
    $GBDK_ROOT = (Join-Path $GBDK_ROOT "win32");
} elseif($IsLinux) {
    $GBDK_ROOT = (Join-Path $GBDK_ROOT "gnu");
} elseif($IsMacOS) {
    $GBDK_ROOT = (Join-Path $GBDK_ROOT "macos");
} else {
    echo "[FATAL] Host is not defined...";
    exit 1;
}


$global:GBDKDIR = $GBDK_ROOT;
$global:LCC     = (Join-Path $global:GBDKDIR "bin/lcc");


##
## Build the Game.
##
if (-not (Test-Path -Path $BUILD_DIR)) {
    echo "Missing ($BUILD_DIR)...";
    mkdir -Force $BUILD_DIR;
}

& $global:LCC                                 `
    -o (Join-Path $BUILD_DIR $EXE_NAME)       `
       (Join-Path $SOURCE_DIR "unit_build.c") `
       (Join-Path $SOURCE_DIR "unit_data.c")
